# Ryan Matthiessen
# CSE262 - Programming Languages
# Homework 5
** Answers are below the questions **

## Question 1
Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

(λp.(pz) λq.((w) λw.(wqzp)))

2. λp.pq λp.qp

(λp.((pq) λp.(qp)))

## Question 2
In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

(((λs.s)z)((λq.s)q))

First λ-expression:
First s bound to λ
second s bound to λs 
z is free

Second λ-expression:
First q bound to λ
s is free
second q is free

2. (λs. s z) λq. w λw. w q z s

(((((λs.s)z)(λq.w)(λw.w)q)z)s)

First λ-expression:
First s bound to λ
second s bound to λs
z is free

Second λ-expression:
q is bound to λ
w is free

Third λ-expression:
First w bound to λ
second w bound to λw
q,z,s are free

3. (λs.s) (λq.qs)

First λ-expression:
First s bound to λ
second s bound to λs

Second λ-expression:
First q bound to λ
second q bound to λq
s is free

4. λz. (((λs.sq) (λq.qz)) λz. (z z))

First λ-expresion (λz.):
First z is bound to λ

Second λ-expression:
First s is bound to λ
second s is bound to λs
q is free

Third λ-expression:
First q is bound to λ
second q is bound to λq
z is free

Fourth λ-expression:
First z is bound to λ
second and third z's are bound to λz

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.
[] for beta-reduction, {} for alpha-conversion
1. (λz.z) (λq.q q) (λs.s a)

(λz.z) (λq.q q) (λs.s a)
(z) (λq.q q) (λs.s a) [z -> (λq.q q)]
(λq.q q) (λs.s a)
(q q) (λs.s a) [q -> (λs.s a)]
(λs.s a) (λs.s a) 
(s a) (λs.s a) [s -> (λs.s a)]
(λs.s a) a {a -> c}
(λs.s c) a [s -> a]
c a

2. (λz.z) (λz.z z) (λz.z q)

(λz.z) (λz.z z) (λz.z q)
(z) (λz.z z) (λz.z q) [z -> (λz.z z)]
(λz.z z) (λz.z q)
(z z) (λz.z q) [z -> (λz.z q)]
(λz.z q) (λz.z q)
(z q) (λz.z q) [z -> (λz.z q)]
(λz.z q) q {q -> p}
(λz.z p) q 
(z p) q [z -> q]
q p

3. (λs.λq.s q q) (λa.a) b

(λs.λq.s q q) (λa.a) b
(λq.s q q) (λa.a) b [s -> (λa.a)]
(λq.(λa.a) q q) b
((λa.a) q q) b [q -> b]
((λa.a) b b)
((a) b b) [a -> b]
((b) b)
b b

4. (λs.λq.s q q) (λq.q) q

(λs.λq.s q q) (λq.q) q
(λq.s q q) (λq.q) q [s -> (λq.q)]
(λq.(λq.q)q q) q {q -> z}
(λq.(λz.z)q q) q
((λz.z)q q) [q -> q]
(z)q q) [z -> q]
(q) q
q q

5. ((λs.s s) (λq.q)) (λq.q)

((λs.s s) (λq.q)) (λq.q)
((s s) (λq.q)) (λq.q) [s -> (λq.q)]
((λq.q) (λq.q)) (λq.q)
((q) (λq.q)) (λq.q) [q -> (λq.q)]
(λq.q) (λq.q)
(q) (λq.q) [q -> (λq.q)]
(λq.q)

## Question 4

1. Write the truth table for the or operator below.

Truth Table
Or(a,b) --> Bool
| a b   |
---------
| T F T |
| T T T |
| F T T |
| F F F |

(λxλy.x) ≡ T
(λxλy.y) ≡ F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

orTT: (λp.λq.p p q)TT

(λp.λq.p p q)TT [T -> (λxλy.x)]
(λp.λq.p p q)(λxλy.x)(λxλy.x)
(λq.p p q)(λxλy.x)(λxλy.x) [p -> (λxλy.x)]
(λq.(λxλy.x) (λxλy.x) q)(λxλy.x)
((λxλy.x) (λxλy.x) q)(λxλy.x) [q -> (λxλy.x)]
(λxλy.x) (λxλy.x) (λxλy.x) {x -> x'}
(λy.x) (λx'λy.x') (λxλy.x) [x -> (λx'λy.x')]
(λy.(λx'λy.x')) (λxλy.x)
(λx'λy.x') (λxλy.x) {x -> x''}
(λy.x') (λx''λy.x'') [x' -> (λx''λy.x'')]
(λy.(λx''λy.x''))
(λx''λy.x'') ≡ T

orTF: (λp.λq.p p q)TF

(λp.λq.p p q)TF
(λq.p p q)TF [p -> T]
(λq.T T q)F
(T T q)F [q -> F]
(T T F)
T ≡ T

or FT: (λp.λq.p p q)FT

(λp.λq.p p q)FT
(λq.p p q)FT [p -> F]
(λq.F F q)T
(F F q)T [q -> T]
(F F T)
T ≡ T

or FF: (λp.λq.p p q)FF

(λp.λq.p p q)FF
(λq.p p q)FF [p -> F]
(λq.F F q)F
(F F q)F [q -> F]
(F F F)
F ≡ F

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

Lambda Expression: (λa.λb.λc.a c b).
This works because the function has one input and has one output. Our outputs will be the opposite of our inputs, therefore, we need to reverse the order in which our outputs are printed in respect to our inputs.
Truth Table
-----------
| T --> F |
| F --> T |
-----------

If statement can return the negation of the boolean entered as appears below:

if (input == true) {
    return false
} else if (input == false) {
    return true
}